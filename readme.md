# Django Crawler Example

## Dependencies

- BeautifulSoup
- Django
- Pillow
- Requests
- TinyMCE
- Django Admin Views

## Installation

First of all, install requirements (with `requirements.txt`) on virtualenv or system-wide

```
pip install -r requirements.txt
```

> Virtualenv is optional but recommended!

## Access to Crawler

If you want to access and insert news from crawler, login to **admin** with visiting `/admin` page.

You will see a link on dashboard; "Crawler - Get News From Ajansspor.com". Click it to access crawler.

Select news for insert with "Bu Haberi Kaydet" checkbox.

Apply changes with submitting form ("Seçilen Haberleri Kaydet")

Check changes on "News"

## Todo

- Reformat Code (!)
- Add comment blocks with docblock
- Change crawler with "CrawlerProvider"
- Add example frontend with exists news