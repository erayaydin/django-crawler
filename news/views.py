from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
import requests
from bs4 import BeautifulSoup
from .models import News, NewsForm
from django.forms import formset_factory
import tempfile
from django.core import files
import datetime
from django.shortcuts import redirect

def index(request):
    return HttpResponse("Frontend of news application.")

def crawler(request):

    if request.POST:
        for item in request.POST.getlist('news'):
            url = item
            fetchItem = requests.get("http://www.ajansspor.com/"+url)
            soup = BeautifulSoup(fetchItem.text, 'html.parser')
            title = soup.select('h1')[0].text
            newsContainer = soup.select('.news-single')[0]
            image = newsContainer.select('img.responsive-img')[0]['src']
            createdAt = newsContainer.select('.news-title span')[0].find(text=True, recursive=False)
            latestUpdate = newsContainer.select('.news-title span time')[0].text
            content = str(newsContainer.select('.description')[0])
            for ad in newsContainer.select('.description')[0].find_all("div", {'class': 'news-in-ad'}):
                ad.decompose()

            imageRequest = requests.get(image, stream=True)
            if(imageRequest.status_code == requests.codes.ok):
                fileName = image.split('/')[-1]
                temp = tempfile.NamedTemporaryFile()
                for block in imageRequest.iter_content(1024 * 8):
                    if not block:
                        break
                    temp.write(block)
            else:
                image = None

            if(latestUpdate):
                latestUpdate = datetime.datetime.strptime(latestUpdate, "%d.%m.%Y %H:%M").strftime("%Y-%m-%d %H:%M")
            if(createdAt):
                createdAt = datetime.datetime.strptime(createdAt, "%d.%m.%Y %H:%M ").strftime("%Y-%m-%d %H:%M")

            newItem = News(name=title, updated_at=latestUpdate, content=content, published_at=createdAt)
            if(image):
                newItem.image.save(fileName, files.File(temp))
            newItem.save()
        return redirect('admin:crawler')

    pageNumber = 1
    url = "http://www.ajansspor.com/Partial/AllNews?Page="

    news = []
    NewsFormSet = formset_factory(NewsForm)
    formset = NewsFormSet()

    totalCrawled = 0
    while totalCrawled < 20:
        fetchUrl = url + str(pageNumber)

        fetch = requests.get(fetchUrl)
        soup = BeautifulSoup(fetch.text, 'html.parser')

        container = soup.select('div[data-page="'+str(pageNumber)+'"]')[0]
        newsList = container.select('div.col.l4.m6.s12')
        totalNews = len(newsList)

        for newsItem in newsList:
            newItemName = newsItem.select('.news-title a')[0].text.strip()
            if News.objects.filter(name=newItemName).count() == 0:
                news.append({
                    'name': newItemName,
                    'image': newsItem.select('img')[0]['src'],
                    'link': newsItem.select('a')[0]['href'],
                })
                totalCrawled += 1

        pageNumber += 1
        if totalNews == 0:
            break

    context = {'title': 'Crawler', 'news': news, 'formset': formset}
    return render(request, "crawler/show.html", context)