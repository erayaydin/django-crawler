# Generated by Django 2.0.7 on 2018-07-31 14:36

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('content', tinymce.models.HTMLField(default=None, null=True)),
                ('image', models.ImageField(default='assets/img/no-img.jpg', upload_to='news-images/')),
                ('published_at', models.DateTimeField(verbose_name='Published At')),
                ('updated_at', models.DateTimeField(default=None, null=True, verbose_name='Updated At')),
            ],
            options={
                'verbose_name': 'News',
                'verbose_name_plural': 'News',
            },
        ),
    ]
