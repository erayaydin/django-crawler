from django.contrib import admin
from django.shortcuts import redirect
from admin_views.admin import AdminViews
from .models import News
from django.contrib.auth.models import User,Group
from .views import crawler

class NewsAdminSite(admin.AdminSite):
    def get_urls(self):
        from django.conf.urls import url
        urls = super(NewsAdminSite, self).get_urls()
        urls = [
            url(r'^crawler/$', self.admin_view(crawler), name='crawler')
        ] + urls
        return urls

class NewsModelAdmin(AdminViews):
    admin_views = (
        ('Crawler - Get News From Ajansspor.com', 'redirect_to_crawler'),
    )

    def redirect_to_crawler(self, *args, **kwargs):
        return redirect('/admin/crawler')

admin_site = NewsAdminSite()
admin_site.register(User)
admin_site.register(Group)
admin_site.register(News, NewsModelAdmin)