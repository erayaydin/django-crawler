from django.db import models
from django.forms import ModelForm
from tinymce.models import HTMLField

class News(models.Model):
    name = models.CharField(max_length=250)
    content = HTMLField(default=None, null=True)
    image = models.ImageField(upload_to='news-images/', default='assets/img/no-img.jpg')
    published_at = models.DateTimeField('Published At')
    updated_at = models.DateTimeField('Updated At', default=None, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'

class NewsForm(ModelForm):
    class Meta:
        model = News
        fields = ['name', 'content', 'image', 'published_at']